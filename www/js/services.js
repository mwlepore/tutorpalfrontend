var SERVER = 'http://warmachine.cse.buffalo.edu'

angular.module('starter.services', ['ngResource'])

    .factory('UserService', function ($resource) {
      return $resource(SERVER+'/userprofiles');
    })
    .factory('ProfileService', function($resource){
        return $resource(SERVER+'/profiles/me');
    })
    .factory('CourseService', function ($resource) {
        return $resource(SERVER+'/courses');
    })
    .factory('MessageService', function($resource){
        return $resource(SERVER+'/mymessages');
    })
    .factory('SendService', function($resource){
        return $resource(SERVER+'/sendmessage')
    })
    .factory('InboxService', function($resource){
        return $resource(SERVER+'/inbox');
    })
    .factory('SentMessageService', function($resource){
        return $resource(SERVER+'/sentmessages')
    })
;
