angular.module('starter', ['ionic', 'starter.controllers','starter.services','ngOpenFB'])

.run(function($ionicPlatform , $rootScope, $timeout, ngFB) {
  ngFB.init({appId: '1709672892689641'});
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

     $rootScope.authStatus = false;
	 //stateChange event
	  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
		  $rootScope.authStatus = toState.authStatus;
		  if($rootScope.authStatus){
			  
			
		  }
    });

	$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
		console.log("URL : "+toState.url);
		if(toState.url=='/dashboard'){
			console.log("match : "+toState.url);
			$timeout(function(){
				angular.element(document.querySelector('#leftMenu' )).removeClass("hide");
			},1000);
		}	
	});

})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

//--------------------------------------

 .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/tab-signin.html'
      }
    },
	authStatus: false
  })
 .state('app.signup', {
    url: '/signup',
    views: {
      'menuContent': {
        templateUrl: 'templates/tab-signup.html'
        ,controller: 'AppCtrl'
      }
   },
	authStatus: false
  })
 .state('app.signupWithFacebook', {
    url: '/signupWithFacebook',
    views: {
      'menuContent': {
        templateUrl: 'templates/signup-after-FBLogin.html'
        ,controller: 'ProfileCtrl'
      }
   },
    authStatus: false
  })
//--------------------------------------



      .state('app.dashboard', {
    cache: false,
          url: '/dashboard',
    views: {
      'menuContent': {
        templateUrl: 'templates/dashboard.html',
		controller: 'ToggleCtrl'
      }
     },
	 authStatus: true
  })
      .state('app.editprofile', {
          url: '/editprofile',
          views: {
              'menuContent': {
                  templateUrl: 'templates/editprofile.html',
                  controller: 'ProfileCtrl'
              }
          },
          authStatus: true
      })
      .state('app.messages', {
          cache: false,
          url: '/messages',
          views: {
              'menuContent': {
                  templateUrl: 'templates/messages.html',
                  controller: 'MessageCtrl'
              }
          },
          authStatus: true
      })
      .state('app.inbox', {
          cache: false,
          url: '/inbox',
          views: {
              'menuContent': {
                  templateUrl: 'templates/inbox.html',
                  controller: 'InboxCtrl'
              }
          },
          authStatus: true
      })
      .state('app.sent', {
          cache: false,
          url: '/sent',
          views: {
              'menuContent': {
                  templateUrl: 'templates/sent.html',
                  controller: 'SentCtrl'
              }
          },
          authStatus: true
      })
      .state('app.findtutor',{
          cache: false,
          url:'/findtutor',
        views:{
          'menuContent': {
            templateUrl: 'templates/findtutor.html',
            controller: 'UsersCtrl'
          }
        },
        authStatus: true
      })

      .state('app.courselist',{
          cache: false,
          url:'/courselist',
          views:{
              'menuContent': {
                  templateUrl: 'templates/Course-List.html',
                  controller: 'ChooseCourse'
              }
          },
          authStatus: true
      })

  .state('app.wallet', {
    url: '/wallet',
    views: {
      'menuContent': {
        templateUrl: 'templates/wallet.html',
    controller: 'DashCtrl'
      }
     },
   authStatus: true
  })

    .state('app.history', {
    url: '/history',
    views: {
      'menuContent': {
        templateUrl: 'templates/history.html',
    controller: 'DashCtrl'
      }
     },
   authStatus: true
  })

      .state('app.settings', {
    url: '/settings',
    views: {
      'menuContent': {
        templateUrl: 'templates/settings.html',
    controller: 'DashCtrl'
      }
     },
   authStatus: true
  })

      .state('app.help', {
    url: '/help',
    views: {
      'menuContent': {
        templateUrl: 'templates/help.html',
    // controller: 'DashCtrl'
      }
     },
   authStatus: true
  })


    .state('app.profiles', {
      url: '/profiles',
      views: {
        'menuContent': {
          templateUrl: 'templates/profiles.html',
          controller: 'ProfilesCtrl'
        }
      }
    })

  .state('app.profile', {
    url: '/profile/:profileId',
    views: {
      'menuContent': {
        templateUrl: 'templates/profile-detail.html',
        controller: 'ProfileCtrl'
      }
    }
  })

      .state('app.map',{
        url:'/map',
        views:{
          'menuContent': {
            templateUrl: 'templates/map.html',
            controller: 'LocationCtrl'
          }
        },
        authStatus: true
      })

    .state('app.addcourses',{
        url:'/AddCourses',
        views:{
            'menuContent': {
                templateUrl: 'templates/AddCourses.html',
                controller: 'AddCourses'
            }
        },
        authStatus: true
    })

      .state('app.sendmessage',{
          url:'/sendmessage/:user',
          views:{
              'menuContent': {
                  templateUrl: 'templates/sendmessage.html',
                  controller: 'SendCtrl'
              }
          },
          authStatus: true
      });


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
