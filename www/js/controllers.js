angular.module('starter.controllers', ['starter.services', 'ngOpenFB', 'ngCordova'])
.controller('AppCtrl', function($scope, $ionicModal, $ionicPopover, $timeout,  $location, $ionicPopup, ngFB, Post, Edit, Message, $http) {


  $scope.loginData = {};

$scope.edit = function(userdata){
    if(typeof(userdata)=='undefined'){
        $scope.showAlert('Please fill in information to proceed.');
        return false;
    }
    else{
        $http({
            method: 'PUT',
            url: 'http://warmachine.cse.buffalo.edu/edit/me',
            data: userdata
        }).then(function successCallback(response) {
            $location.path('/app/dashboard');
            //$scope.showAlert('Profile successfully updated!');
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });

    }
}

$scope.signup = function(FBuser, user){
    user.firstname = FBuser.first_name;
    user.lastname = FBuser.last_name;
    user.facebook_id = FBuser.id;
    var str1 = "http://graph.facebook.com/";
    var res = str1.concat(user.facebook_id);

    //user.image_url = "http://graph.facebook.com/{{user.facebook_id}}/picture?width=150&height=150";
    user.image_url = res.concat("/picture?width=150&height=150");
    if(typeof(user)=='undefined'){
        $scope.showAlert('Please fill in information to proceed.');
        return false;
    }
    else{
        $scope.showAlert(user);
        var x = new Post(user);
        x.$save(function(postObject) {
            alert(JSON.stringify(postObject));
        });
    return true;}

}


  //--------------------------------------------

  $scope.facebookSignIn = function () {
    ngFB.login({scope: 'email,public_profile,publish_actions'}).then(
        function (response) {
            if (response.status === 'connected') {
                console.log('Facebook login succeeded');
                ngFB.api({
                    path: '/me',
                    params: {fields: 'id,name,gender,first_name,last_name'}
                }).then(
                    function (user) {
                        $scope.FBuser = user;
                        $http.defaults.headers.common.Authorization = "Bearer facebook "+window.sessionStorage.fbAccessToken;
                        var x = {};
                        x.firstname = $scope.FBuser.first_name;
                        x.lastname = $scope.FBuser.last_name;
                        x.facebook_id = $scope.FBuser.id;
                        var str1 = "http://graph.facebook.com/";
                        var res = str1.concat(x.facebook_id);
                        x.image_url = res.concat("/picture?width=150&height=150");
                        $http({
                            method: 'PUT',
                            url: 'http://warmachine.cse.buffalo.edu/edit/me',
                            data: x
                        }).then(function successCallback(response) {
                            $location.path('/app/dashboard');
                         //   $scope.closeLogin();
                        }, function errorCallback(response) {
                        })
                    },
                    function (error) {
                        alert('Facebook error: ' + error.error_description);
                    });
            }
            else {
                alert('Facebook login failed');
            }
        });
	};

    $scope.facebookSignUp = function () {
    ngFB.login({scope: 'email,public_profile,publish_actions'}).then(
        function (response) {
            if (response.status === 'connected') {
                console.log('Facebook login succeeded');
                $location.path('/app/signupWithFacebook');
               // $scope.closeLogin();
            } else {
                alert('Facebook login failed');
            }
        });
    };






	$scope.login = function(user) {
			
		if(typeof(user)=='undefined'){
			$scope.showAlert('Please fill username and password to proceed.');	
			return false;
		}

		if(user.username=='demo@g mail.com' && user.password=='demo'){
			$location.path('/app/dashboard');
		}else{
			$scope.showAlert('Invalid username or password.');	
		}
		
	};
  //--------------------------------------------
  $scope.logout = function() {   $location.path('/app/login');   };
  //--------------------------------------------
   // An alert dialog
	 $scope.showAlert = function(msg) {
	   var alertPopup = $ionicPopup.alert({
		 title: 'Warning Message',
		 template: msg
	   });
	 };
  //--------------------------------------------
})
    .factory('Post', function($resource) {
        return $resource('http://warmachine.cse.buffalo.edu/userprofiles/new/');
    })

    .factory('Edit', function($resource) {
        return $resource('http://warmachine.cse.buffalo.edu/edit/me/');
    })

    .factory('Message', function($resource){
        return $resource('http://warmachine.cse.buffalo.edu/sendmessage')
    })

    .controller('LocationCtrl', function ($scope, $cordovaGeolocation, $ionicPlatform, $ionicPopup, UserService, CourseService, $timeout, choose_course, ProfileService) {

        var map;
        var mycourse = choose_course.get_course();


        function initMap(coords) {
            // Create a map object and specify the DOM element for display.
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: coords.latitude, lng: coords.longitude},
                scrollwheel: false,
                zoom: 16
            });


            google.maps.event.addListenerOnce(map, 'idle', function () {


                $scope.profile = ProfileService.get();

                var userprofiles = UserService.query(function () {

                    var count = 0;

                    userprofiles.forEach(function () {
                        var user = userprofiles[count++];
                        var raw_courselist = user.subject;
                        var raw_course = "";
                        var courses = [];

                        for (var i = 0; i <= raw_courselist.toString().length; i++) {
                            if (raw_courselist.toString().charAt(i) != ' ' && i != raw_courselist.toString().length) {
                                raw_course = raw_course + raw_courselist.toString().charAt(i);
                            }
                            else {
                                courses.push(raw_course);
                                raw_course = "";
                            }
                        }

                        var count2 = 0;
                        var flag = 0;
                        courses.forEach(function () {
                            var course = courses[count2++];
                            if (course == mycourse) {
                                flag = 1;
                            }
                        })


                        if (user.active == true && flag == 1) {
                            var messagelink = "#/app/sendmessage/"+user.user+"";
                            var marker = new google.maps.Marker({
                                map: map,
                                //animation: google.maps.Animation.DROP,
                                title: user.firstname + " " + user.lastname,
                                position: {lat: user.latitude, lng: user.longitude},
                                clickable: true
                            });
                            marker.info = new google.maps.InfoWindow({
                                content: '<center>' + '<div id="iw-container">' +
                                '<div class="iw-title"><h4>' + user.firstname + " " + user.lastname + '</h4></div>' +
                                '<div class="iw-content">' +
                                '<img style="border:1px solid black;" alt="Doge pic" height="100" width="100"; src="http://graph.facebook.com/' + user.facebook_id + '/picture?width=100&height=100">' + '</center>' +
                                '<div class="iw-subTitle"><b>About me</b></div>' +
                                '<p>' + user.aboutme + '</p>' +
                                '<b>Rate: </b>' + "$" + user.rate + "/hour" + '<br><b>Languages: </b>' + user.language + '</p>' +
                                '</div>' + '<center>' + '<a href=' + messagelink +'><button style="background-color:#499bea;color: white; font-size: larger;height:50px;width:150px;border: 1pt ridge black">Message Me!</button>' + '</center>' +
                                '<div class="iw-bottom-gradient"></div>' +
                                '</div>'
                            });

                            google.maps.event.addListener(marker, 'click', function () {
                                //this = marker
                                var marker_map = this.getMap();
                                this.info.open(marker_map);
                                this.info.open(marker_map, this);
                                // Note: If you call open() without passing a marker, the InfoWindow will use the position specified upon construction through the InfoWindowOptions object literal.
                            });
                        }
                    })
                });

            });
        }


        $ionicPlatform.ready(function () {
            var posOptions = {timeout: 10000, enableHighAccuracy: true};
            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function (position) {
                    $scope.coords = position.coords;
                    initMap(position.coords)
                }, function (err) {
                    // error
                });
        });

    })



    .service('choose_course', function(){
        var course;

        this.get_course = function(){
            return course;
        }

        this.change_course = function(new_course){
            course = new_course;
        }
    })

    .controller('ChooseCourse', function($scope, CourseService, choose_course){

        $scope.courselist = CourseService.query();

        $scope.GetCourse = function(){
            $scope.mycourse = choose_course.get_course();
        }

        $scope.ChangeCourse = function(newCourse){
            choose_course.change_course(newCourse);
        }


    })

    .controller('AddCourses', function($scope, CourseService, ProfileService, Edit){

        $scope.profile = ProfileService.get();
        $scope.courselist = CourseService.query();


        $scope.AddCourse = function(profile, course){

            var raw_courselist = profile.subject;
            var raw_course = "";
            var courses = [];

            for (var i = 0; i <= raw_courselist.toString().length; i++) {
                if (raw_courselist.toString().charAt(i) != ' ' && i != raw_courselist.toString().length) {
                    raw_course = raw_course + raw_courselist.toString().charAt(i);
                }
                else {
                    courses.push(raw_course);
                    raw_course = "";
                }
            }


            var flag = false;
            var count = 0;
            courses.forEach(function(){
                if(courses[count++] == course){
                    flag = true;
                }
            })

            if(flag == false){

                var index = courses.indexOf("None");

                if (index > -1) {
                    profile.subject = course;
                }
                else{
                    profile.subject = profile.subject + " " + course;
                }
            }
        }

    })

    .controller('ToggleCtrl', function ($scope, Edit, ProfileService, get_coords) {

        $scope.profile = ProfileService.get();
        get_coords.change_coords();

        $scope.getCourses = function(profile){
            var courses = []
            var raw_courselist = profile.subject;
            var raw_course = "";

            for (var i = 0; i <= raw_courselist.toString().length; i++) {
                if (raw_courselist.toString().charAt(i) != ' ' && i != raw_courselist.toString().length) {
                    raw_course = raw_course + raw_courselist.toString().charAt(i);
                }
                else {
                    courses.push(raw_course);
                    raw_course = "";
                }
            }
            $scope.courselist = courses;
        }

        $scope.removeCourse = function(profile, course, courselist){

            var index = courselist.indexOf(course);

            if (index > -1) {
                courselist.splice(index, 1);
            }

            var courses = "";
            var count = 0;
            courselist.forEach(function(){
                courses = courses + " " + courselist[count++];
            })

            if(courses != "") {
                profile.subject = courses;
            }
            else{
                profile.subject = "None";
            }
        }

        $scope.updateProfile = function (user, active) {

            if(active == true) {
                user.latitude = get_coords.get_lat();
                user.longitude = get_coords.get_long();
            }
            else{
                user.latitude = 0;
                user.longitude = 0;
            }
        }

    })

    .service('get_coords', function($cordovaGeolocation) {
        var lat = 0;
        var long = 0;

        function updateData(coords) {
            lat = coords.latitude;
            long = coords.longitude;
        }

        this.get_lat = function () {
            return lat;
        }

        this.get_long = function () {
            return long;
        }

        this.change_coords = function () {
            var posOptions = {timeout: 10000, enableHighAccuracy: true};

            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function (position) {

                    updateData(position.coords);

                }, function (err) {
                    // error
                })
        }
    })

	.controller('ProfileCtrl', function ($scope, ProfileService) {
    $scope.profile = ProfileService.get();
})

	//Gets all users.
	.controller('UsersCtrl', function($scope, UserService){
		$scope.userprofiles = UserService.query();
	})


    .controller('MessageCtrl', function($scope, MessageService){
        $scope.messagelist = MessageService.query();
    })

    .controller('InboxCtrl', function($scope, InboxService){
        $scope.inboxlist = InboxService.query();
    })

    .controller('SentCtrl', function($scope, SentMessageService){
        $scope.sentlist = SentMessageService.query();
    })

    .controller('SendCtrl', function($scope, ProfileService, $stateParams, SendService){
        $scope.sendto = $stateParams.user;
        $scope.profile = ProfileService.get();

        $scope.send = function(body){
            var mess = {};
            mess.body = body;
            mess.recipient = $scope.sendto;
            mess.sender = $scope.profile.user;
            var x = new SendService(mess);
            x.$save(function(postObject) {
                alert(JSON.stringify(postObject));
            });
        }
    });