# cse442TeamJAM
Tutor matchmaker app.  An app that helps students find potential tutors nearby, and tutors to find potential students nearby.  This app will also have functionality that integrates with social media, provide help on homework, and include payments from students to tutors.

Contributors:
Matt - mwlepore
Adi - msaditya
Jared - jaredpay