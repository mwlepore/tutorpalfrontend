package com.example.drawkward.cse442teamjam;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by drawkward on 9/17/16.
 */
public class SwipeAdapter extends PagerAdapter {

    private int[] imageSlider = {R.drawable.tutor_profile_jarred, R.drawable.tutor_profile_michael, R.drawable.tutor_profile_tamara};

    private Context context;
    private LayoutInflater layoutInflater;

    public SwipeAdapter(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return imageSlider.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==(LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item_view = layoutInflater.inflate(R.layout.swipe_layout, container,false);

        ImageView image_view = (ImageView) item_view.findViewById(R.id.image_view);

        image_view.setImageResource(imageSlider[position]);
        container.addView(item_view);
        return item_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
