package com.example.drawkward.cse442teamjam;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ProfilePage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_page);
    }

    public void TutorBtn(View view){
        Intent TutorPage = new Intent(this, TutorPage.class);
        startActivity(TutorPage);
    }

}
